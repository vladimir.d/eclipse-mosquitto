FROM ubuntu:focal

# Set terminal to be noninteractive
ENV DEBIAN_FRONTEND noninteractive

# Damned, infernal sources time/bandwidth wastage!
RUN sed -i 's/deb-src/# deb-src/' /etc/apt/sources.list

# Put main packages in place
RUN apt-get update && \
    apt-get upgrade -y -f && \
    apt-get install --no-install-recommends --allow-unauthenticated -y -f \
        sudo \
        curl \
		gnupg \
		ca-certificates \
		mosquitto \
		mosquitto-clients \
		supervisor \
		nginx \
		vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# copy configs
ADD config/mosquitto /app/data/mosquitto/config
RUN mkdir -p /app/data/mosquitto/log && \
	chown -R mosquitto:mosquitto /app/data/mosquitto/log && \
	rm -rf /etc/mosquitto && \
	ln -s /app/data/mosquitto/config /etc/mosquitto

# nginx
ADD config/nginx /app/data/nginx
RUN rm -rf /etc/nginx/sites-enabled && \
	ln -s /app/data/nginx/conf/sites-enabled /etc/nginx/sites-enabled && \
	rm -rf /var/lib/nginx/ && \
	mkdir -p /run/nginx && \
	ln -sf /run/nginx /var/lib/nginx && \
	ln -sf /run/nginx-error.log /var/log/nginx/error.log && \
	ln -sf /run/nginx-access.log /var/log/nginx/access.log

EXPOSE 1883 9001 80

# supervisor
# ADD config/supervisor/ /etc/supervisor/conf.d/
ADD config/supervisor/ /app/data/supervisor/
RUN rm -rf /etc/supervisor/supervisord.conf && \
	rm -rf /etc/supervisor/conf.d/ && \
	ln -sf /app/data/supervisor/supervisord.conf /etc/supervisor/supervisord.conf && \
	ln -sf /app/data/supervisor/conf.d /etc/supervisor/conf.d && \
	ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
