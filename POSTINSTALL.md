A test user created:

Username: test

Password: test-one-two-3


To remove a test Mosquitto user, run the following in the terminal::

```
$ mosquitto_passwd -D /etc/mosquitto/pwfile test
```


To add a new Mosquitto user:

```
$ mosquitto_passwd -b /etc/mosquitto/pwfile NEWUSER PASSWORD
```

Three directories have been created in the application to be used for configuration, persistent storage and logs.

```
/app/data/mosquitto/config

/app/data/mosquitto/data

/app/data/mosquitto/log
```
