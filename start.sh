#!/bin/bash

#if [[ ! -d /app/data/default ]]
#then

    # create this is first install, so setup /app/data and initial settings
#    mkdir -p /app/data/default
#fi

echo "Starting Mosquitto"

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Mosquitto
