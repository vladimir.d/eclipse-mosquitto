# Cloudron App

This directory contains Cloudron files for Mosquitto.


It contains a generic config that can be used to build Mosquitto app
based on the ubuntu:focal image


Init image tag

```
Tag=cloudron-$(date +%s)
```


To build and push image to Gitlab registry:

```
$ docker build -f Dockerfile --no-cache . -t registry.gitlab.com/brandlight/cloudron/mosquitto:$Tag && \
	docker push registry.gitlab.com/brandlight/cloudron/mosquitto:$Tag
```


To install app on Cloudron:

```
$ cloudron install --no-wait --server YOUR-CLOUDRON-SERVER --location mosquitto --image registry.gitlab.com/brandlight/cloudron/mosquitto:$Tag --token YOUR-GITLAB-TOKEN
```

Update app on Cloudron:

```
$ cloudron update --no-wait --no-backup --server YOUR-CLOUDRON-SERVER --app mosquitto  --image registry.gitlab.com/brandlight/cloudron/mosquitto:$Tag --token YOUR-GITLAB-TOKEN
```

To remove image from registry:

```
$ docker rmi -f registry.gitlab.com/brandlight/cloudron/mosquitto:$Tag
```

# Dynamic Security Plugin

The Dynamic Security plugin is a Mosquitto plugin which provides role based authentication and access control features that can updated whilst the broker is running, using a special topic based API.

[Documentation] (https://mosquitto.org/documentation/dynamic-security/)


[Username / Password Example] (http://www.steves-internet-guide.com/mqtt-username-password-example/) 

Default test user created:

Username: test

Password: test-one-two-3


To remove a test Mosquitto user:

```
$ mosquitto_passwd -D /etc/mosquitto/pwfile test
```


To add a new Mosquitto user:

```
$ mosquitto_passwd -b /etc/mosquitto/pwfile NEWUSER PASSWORD
```
